<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('offers', 'API\IndexOffersController@index');
Route::get('offers/{offer}', 'API\IndexOffersController@show');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'API\AuthController@logout');
    });
});

Route::group([
    'middleware' => 'auth:api'
  ], function() {
    Route::get('offer/show/{id}/', 'API\OfferController@show');  
    Route::delete('offer/delete/{offer}/', 'API\OfferController@delete'); 
    Route::post('offer/store', 'API\OfferController@store');
});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'author_user_id', 'title', 'name', 'description'
    ];

    protected $with = [
        'category'
    ];
    
    /**
     * Category relation
     */
    public function category() {
        return $this->belongsToMany('App\Category', 'offer_categories', 'offer_id', 'category_id')->withTimestamps();
    }

}

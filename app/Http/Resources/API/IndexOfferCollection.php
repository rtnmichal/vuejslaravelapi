<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Category;

class IndexOfferCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $categories = Category::select('id', 'name')->get();
        return [
            'myOffers' => $this->collection,
            'categories' => $categories
        ];
    }
}

<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Offer;
use App\Http\Resources\API\OfferCollection;
use App\Http\Requests\API\StoreOfferRequest;

class OfferController extends Controller
{
    /**
     * @param int $id
     * @queryParam id required user id
     * @return array $offers
     */
    public function show($id) {
        if(Auth::id() == $id) {
            $offers = Offer::where('author_user_id', $id)->get();
        } else {
            return response()->json(['message' => 'unauthorized'], 401);
        }

        // return response()->json($offers);
        return new OfferCollection($offers);
    }

    /**
     * @param  \App\Http\Requests\API\StoreOfferRequest $request
     * @bodyParam author_user_id integer required user id
     * @bodyParam title string required offer title
     * @bodyParam description string required offer description
     * @bodyParam category integer requried category id
     * @return string message 
     */
    public function store(StoreOfferRequest $request) {
            $request = json_decode($request->getContent(), true); 
            $offer = Offer::create([
                'author_user_id' => $request['author_user_id'], 
                'title' => $request['title'], 
                'description' => $request['description'],
            ]);
            $offer->category()->sync($request['category']);
            $offer->save();

            return response()->json($offer);
    }

    /**
     * @param \App\Offer $offer
     * @queryParam offer required id of offer
     * @return string response()
     */
    public function delete(Offer $offer) {
        if(Auth::id() == $offer->author_user_id) {
            $offer->delete();
        } else {
            return response()->json(['message' => 'unauthorized'], 401);
        }
        return response()->json(['message' => 'Your offer was deleted sucessfully.']);
    }
}

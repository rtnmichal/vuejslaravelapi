<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use App\Http\Resources\API\IndexOfferCollection;

class IndexOffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        if (isset($request['category_name'])) {
            $offers = Offer::select('id', 'title', 'description', 'created_at')
            ->whereHas('Category',  function($q) use ($request){
                $q->where('name', '=', $request['category_name'] );
            })
            ->orderBy('created_at', 'DESC')
            ->get();
        } else {
            $offers = Offer::select('id', 'title', 'description', 'created_at')
            ->orderBy('created_at', 'DESC')
            ->get();
        }

        return new IndexOfferCollection($offers);
    }

    /**
     * Display the specified resource.
     * @queryParam offer required offer to show
     * @param  \App\Offer $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        return response()->json($offer);
    }

}

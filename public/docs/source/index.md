---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_2741c31dd89e5d192b39977e6ec69bb2 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/offers" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/offers",
    "method": "GET",
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/offers`


<!-- END_2741c31dd89e5d192b39977e6ec69bb2 -->

<!-- START_512ca1cc43ac8514662aba3aba6157db -->
## Display the specified resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/offers/{offer}" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/offers/{offer}",
    "method": "GET",
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/offers/{offer}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    offer |  required  | offer to show

<!-- END_512ca1cc43ac8514662aba3aba6157db -->

<!-- START_a925a8d22b3615f12fca79456d286859 -->
## Login user and create token

> Example request:

```bash
curl -X POST "http://localhost/api/auth/login" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
 \
    -d "email"="wUtxhAnBQsLAV4Qx" \
        -d "password"="9kqtleHiiomxinzV" \
        -d "remember_me"="1" ```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/auth/login",
    "method": "POST",
    "data": {
        "email": "wUtxhAnBQsLAV4Qx",
        "password": "9kqtleHiiomxinzV",
        "remember_me": true
    },
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/auth/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | email
    password | string |  required  | password
    remember_me | boolean |  optional  | optional it's set auto to remember

<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_9357c0a600c785fe4f708897facae8b8 -->
## Create user

> Example request:

```bash
curl -X POST "http://localhost/api/auth/signup" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
 \
    -d "name"="94nUCcrIImiZBb0R" \
        -d "email"="ahuiIJOCDfQ6Xmh4" \
        -d "password"="XQq74wWZl5KKSdhF" \
        -d "password_confirmation"="pTOHMnT2nl7kis9U" ```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/auth/signup",
    "method": "POST",
    "data": {
        "name": "94nUCcrIImiZBb0R",
        "email": "ahuiIJOCDfQ6Xmh4",
        "password": "XQq74wWZl5KKSdhF",
        "password_confirmation": "pTOHMnT2nl7kis9U"
    },
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/auth/signup`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | name
    email | string |  required  | email
    password | string |  required  | password
    password_confirmation | string |  required  | pw confirmation

<!-- END_9357c0a600c785fe4f708897facae8b8 -->

<!-- START_16928cb8fc6adf2d9bb675d62a2095c5 -->
## Logout user (Revoke the token)

> Example request:

```bash
curl -X GET -G "http://localhost/api/auth/logout" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/auth/logout",
    "method": "GET",
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/auth/logout`


<!-- END_16928cb8fc6adf2d9bb675d62a2095c5 -->

<!-- START_035d6a745112d4fd8deee30219168318 -->
## api/offer/show/{id}
> Example request:

```bash
curl -X GET -G "http://localhost/api/offer/show/{id}" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/offer/show/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/offer/show/{id}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    id |  required  | user id

<!-- END_035d6a745112d4fd8deee30219168318 -->

<!-- START_eec9dee546a74236169c6709f6ce5993 -->
## api/offer/delete/{offer}
> Example request:

```bash
curl -X DELETE "http://localhost/api/offer/delete/{offer}" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/offer/delete/{offer}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/offer/delete/{offer}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    offer |  required  | id of offer

<!-- END_eec9dee546a74236169c6709f6ce5993 -->

<!-- START_4a11fa01d1929a36bf4278d9f09a2e54 -->
## api/offer/store
> Example request:

```bash
curl -X POST "http://localhost/api/offer/store" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer: {token}" 
 \
    -d "author_user_id"="4" \
        -d "title"="Wo3mh9mNkwn8pKNS" \
        -d "description"="6KGuC5GSblsMkoJ4" \
        -d "category"="8" ```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/offer/store",
    "method": "POST",
    "data": {
        "author_user_id": 4,
        "title": "Wo3mh9mNkwn8pKNS",
        "description": "6KGuC5GSblsMkoJ4",
        "category": 8
    },
    "headers": {
        "accept": "application/json",
        "Authorization": "Bearer: {token}",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/offer/store`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    author_user_id | integer |  required  | user id
    title | string |  required  | offer title
    description | string |  required  | offer description
    category | integer |  optional  | requried category id

<!-- END_4a11fa01d1929a36bf4278d9f09a2e54 -->



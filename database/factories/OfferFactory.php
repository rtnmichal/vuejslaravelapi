<?php

use Faker\Generator as Faker;

$factory->define(App\Offer::class, function (Faker $faker) {
    return [
        'author_user_id' => 1,
        'title' => $faker->text(100),
        'description' => $faker->text(500)
    ];
});

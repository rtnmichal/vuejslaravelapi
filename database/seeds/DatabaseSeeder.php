<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Offer::class, 25)->create()->each(function($offer){
            $category = factory(App\Category::class)->make();
            $offer->category()->save($category);
         });
    }
}
